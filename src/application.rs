use gio::prelude::*;
use gtk::prelude::*;
use std::cell::RefCell;
use std::env;
use std::path::PathBuf;
use std::rc::Rc;
use std::thread;

use crate::config;
use crate::exporter::Exporter;
use crate::window::Window;

pub struct Application {
    app: gtk::Application,
    window: Window,
}

impl Application {
    pub fn new() -> Self {
        let app = gtk::Application::new(Some(config::APP_ID), gio::ApplicationFlags::FLAGS_NONE).unwrap();
        let window = Window::new();

        glib::set_application_name(&format!("VectorSlicer{}", config::NAME_SUFFIX));
        glib::set_prgname(Some("vector-slicer"));

        let builder = gtk::Builder::new_from_resource("/org/gnome/design/VectorSlicer/shortcuts.ui");
        let dialog: gtk::ShortcutsWindow = builder.get_object("shortcuts").unwrap();
        window.widget.set_help_overlay(Some(&dialog));

        let application = Self { app, window };

        application.setup_gactions();
        application.setup_signals();
        application.setup_css();
        application
    }

    pub fn setup_gactions(&self) {
        let output_file: Rc<RefCell<Option<PathBuf>>> = Rc::new(RefCell::new(None));
        // Quit
        let app = self.app.clone();
        self.add_gaction("quit", move |_, _| app.quit());
        self.app.set_accels_for_action("app.quit", &["<primary>q"]);

        // About
        let window = self.window.widget.clone();
        self.add_gaction("about", move |_, _| {
            let builder = gtk::Builder::new_from_resource("/org/gnome/design/VectorSlicer/about_dialog.ui");
            let about_dialog: gtk::AboutDialog = builder.get_object("about_dialog").unwrap();
            about_dialog.set_transient_for(Some(&window));

            about_dialog.connect_response(|dialog, _| dialog.destroy());
            about_dialog.show();
        });
        self.app.set_accels_for_action("app.about", &["<primary>comma"]);
        let (tx, rx) = glib::MainContext::channel(glib::PRIORITY_DEFAULT);
        // File open
        let window = self.window.widget.clone();
        let weak_file_button = self.window.dest_button.downgrade();
        let weak_ready_button = self.window.ready_button.downgrade();
        let weak_ready_label = self.window.ready_label.downgrade();
        let weak_output_file = Rc::downgrade(&output_file);
        self.add_gaction("openfile", move |_, _| {
            let file_chooser = gtk::FileChooserNative::new(Some("Open SVG"), Some(&window), gtk::FileChooserAction::Open, Some("_Select"), Some("_Cancel"));

            // TODO: ADD svg filter
            //file_chooser.add_filter(f);

            let response = file_chooser.run();
            if gtk::ResponseType::from(response) == gtk::ResponseType::Accept {
                let dest_button = weak_file_button.upgrade().unwrap();
                if let Some(src_file) = file_chooser.get_filename() {
                    if let Some(mut dest_file) = dest_button.get_filename() {
                        let filename = src_file.file_stem().map(|x| x.to_os_string()).unwrap();
                        let exporter = Exporter::new(src_file);

                        dest_file.push(filename);
                        dest_file.set_extension("pdf");
                        let output_file = weak_output_file.upgrade().unwrap();
                        *output_file.borrow_mut() = Some(dest_file.clone());
                        let tx = tx.clone();
                        thread::spawn(move || {
                            exporter.generate_pdf(dest_file.as_path(), tx);
                        });
                    }
                }
            }
        });
        self.app.set_accels_for_action("app.openfile", &["<primary>o"]);

        let weak_progress_bar = self.window.progress_bar.downgrade();
        rx.attach(None, move |fraction| {
            let progress_bar = weak_progress_bar.upgrade().unwrap();
            progress_bar.set_fraction(fraction);
            let ready_label = weak_ready_label.upgrade().unwrap();
            let ready_button = weak_ready_button.upgrade().unwrap();
            if fraction == 1. {
                ready_label.show();
                ready_button.show();
            } else {
                ready_label.hide();
                ready_button.hide();
            }
            glib::Continue(true)
        });
        let window = self.window.widget.clone();
        self.add_gaction("openpdf", move |_, _| {
            if let Some(ref file) = *output_file.borrow() {
                let _ = gtk::show_uri_on_window(Some(&window), &format!("file://{}", file.to_str().unwrap()), gtk::get_current_event_time());
            }
        });
    }

    pub fn setup_signals(&self) {
        let window = self.window.widget.clone();
        self.app.connect_activate(move |app| {
            window.set_application(Some(app));
            app.add_window(&window);
            window.present();
        });
    }

    fn add_gaction<F>(&self, name: &str, action: F)
    where
        for<'r, 's> F: Fn(&'r gio::SimpleAction, Option<&'s glib::Variant>) + 'static,
    {
        let simple_action = gio::SimpleAction::new(name, None);
        simple_action.connect_activate(action);
        self.app.add_action(&simple_action);
    }

    pub fn setup_css(&self) {
        let p = gtk::CssProvider::new();
        gtk::CssProvider::load_from_resource(&p, "/org/gnome/design/VectorSlicer/style.css");
        gtk::StyleContext::add_provider_for_screen(&gdk::Screen::get_default().unwrap(), &p, 500);
    }

    pub fn run(&self) {
        info!("VectorSlicer{} ({})", config::NAME_SUFFIX, config::APP_ID);
        info!("Version: {} ({})", config::VERSION, config::PROFILE);
        info!("Datadir: {}", config::PKGDATADIR);

        let args: Vec<String> = env::args().collect();
        self.app.run(&args);
    }
}
